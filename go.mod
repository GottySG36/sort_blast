module bitbucket.org/GottySG36/sort_blast

go 1.13

require (
	bitbucket.org/GottySG36/blast/v2 v2.5.0
	bitbucket.org/GottySG36/rank v0.6.0
	bitbucket.org/GottySG36/sequtils v0.18.0
	bitbucket.org/GottySG36/utils v0.3.1
)
