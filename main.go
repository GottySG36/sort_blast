// Utility to sort and extract the best blast match

package main

import (
    "flag"
    "fmt"
    "log"
    "os"
    "strings"
    "sync"
    "time"

    blast "bitbucket.org/GottySG36/blast/v2"
    "bitbucket.org/GottySG36/rank"
    "bitbucket.org/GottySG36/sequtils"
    "bitbucket.org/GottySG36/utils"
)

var (
    inp  = flag.String("i", "", "Input blast files, comma seperated")
    fna  = flag.String("f", "", "Input fasta files, comma seperated")
    outp = flag.String("o", "", "Output directories, comma seperated, in which to extract the most abundant taxids' genomes")
    pref = flag.String("p", "sample", "Prefix used to build output file names, has to be a comma seperated list if multiple input files")
    columns = flag.String("cols", "", "List of columns found in the blast result files")
    sort = flag.String("fields", "", "Fields to sort, in order of importance (see full description below to know which are available)")
    topX = flag.Int("top", 20, "Number of the most abundant taxids to keep (0 or <0 for all)")
    bla  = flag.Int("b", 0, "Toggle to print the resulting sorted Blast table")
    dup  = flag.Bool("d", false, "Will show if there were other best hits")
    res  = flag.Bool("r", false, "Toggle to print the number of sequences found for each taxid from the top X sequences")
    rnk  = flag.Bool("rk", false, "Toggle to print the average ranks for the top X sequences")
    ident = flag.Float64("p-ident", 0, "Blastp minimum identity percentage")
    filtKing = flag.String("k", "", "Terms to ignore in row 'Kingdom', comma seperated")
    filtNames = flag.String("s", "", "Terms to ignore in row 'Scinames', comma seperated")
)

func main() {
    flag.Parse()
    files := strings.Split(*inp, ",")
    fastas := strings.Split(*fna, ",")

    if *filtKing == "" {
        *filtKing = "SHOULDNEVERMATCH"
    }
    if *filtNames == "" {
        *filtNames = "SHOULDNEVERMATCH"
    }

    if *inp == "" {
        log.Fatalf("Error -:- Input : No blast files specified. At least one file must be specified with option '-i'\n")
    }

    lPref := strings.Split(*pref, ",")
    if len(lPref) != 1 && len(lPref) != len(files) {
        log.Fatal("Error -:- Prefix : Number of prefix names does not match the number of input files\n")
    } else if *pref == "" {
        log.Fatal("Error -:- Option '-p' used without any argument\n")
    } else if len(lPref) == 1 {
        for i := 1; i < len(files); i++ {
            lPref = append(lPref, lPref[0])
        }
    }

    out := []string{}
    if *outp != "" {
        out = strings.Split(*outp, ",")
    }
    if ( len(out) != 0 && len(out) != len(files) ) && ( *rnk == false && *res == false && *dup == false && *bla == 0 ) {
        log.Fatalf("Error -:- Output : Number of output directories does not match the number of input files\n")
    }


    var wg sync.WaitGroup
    var lock sync.Mutex
    blastRes := make(map[string]blast.Blast, len(files))
    sortedBlastDup := make(map[string]blast.Duplicated, len(files))
    // {File :{Taxid:{Headers:[...]}, ... }, ...}
    sortedBlast := make(map[string]map[string]map[string]bool, len(files))
    sortedTaxo := make(map[string]rank.Counts, len(files))



    c := strings.Split(*columns, ",")
    quit := make(chan struct{})
    go utils.Spinner("Loading", 100, quit)
    for _, file := range files {
        wg.Add(1)

        go func(file string, columns []string) {
            defer wg.Done()

            b, err := blast.LoadBlast(file, columns)
            if err != nil {
                fmt.Fprintf(os.Stderr, "Error -:- LoadBlast : %v\n", err)
            }

            lock.Lock()
            blastRes[file] = b
            lock.Unlock()

        }(file, c)
    }
    wg.Wait()
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf(" Done!\n\n")

    quit = make(chan struct{})
    go utils.Spinner("Sorting", 100, quit)
    for file, bl := range blastRes {
        wg.Add(1)
        go func(file string, bl blast.Blast, order string) {
            defer wg.Done()

            srt, err := blast.ListSortClosures(order)
            if err != nil {
                log.Fatalf("Error -:- ListSortClosures : %v\n", err)
            }

            blast.OrderedBy(srt).Sort(bl)
            e, dup, err := blast.ExtractBest(bl, *ident, *filtKing, *filtNames)
            if err != nil {
                log.Fatalf("Error -:- ExtractBest : %v\n", err)
            }

            g, err := e.ExtractGenomes()
            if err != nil {
                log.Fatalf("Error -:- ExtractGenomes : %v\n", err)
            }
            t := rank.SortCounts(g)

            lock.Lock()
            blastRes[file] = e
            sortedBlast[file] = g
            sortedBlastDup[file] = dup
            sortedTaxo[file] = t
            lock.Unlock()
        }(file, bl, *sort)
    }
    wg.Wait()
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf(" Done!\n\n")

    avgRank := make(map[string]float64)
    nbAvgRank := make(map[string]float64)
    for _, c := range sortedTaxo {
        for _, taxo := range c {
            avgRank[taxo.Taxid] += float64(taxo.Rank)
            nbAvgRank[taxo.Taxid] += 1
        }
    }

    for t, _ := range avgRank {
        avgRank[t] /= nbAvgRank[t]
    }
    t20Sort := rank.SortRanks(avgRank)
    if *topX > len(t20Sort) || *topX <= 0 {
        *topX = len(t20Sort)
    }

    if len(out) == len(files) && len(fastas) == len(out) && *fna != "" {
        quit = make(chan struct{})
        go utils.Spinner("Extracting", 100, quit)
        for idx, file := range files {
            wg.Add(1)

            go func(inf, outdir string, t rank.Ranks, heads map[string]map[string]bool) {
                defer wg.Done()
                for i := 0; i<*topX; i++ {
                    err := sequtils.ExtractFna(inf, outdir, t[i].Taxid, heads[t20Sort[i].Taxid])
                    if err != nil {
                        log.Fatalf("Error -:- extractFna : %v\n", err)
                    }
                }

            }(fastas[idx], out[idx], t20Sort, sortedBlast[file])
        }
        wg.Wait()
        close(quit)
        time.Sleep(250 * time.Millisecond)
        fmt.Printf(" Done!\n\n")
    }

    if *rnk && *outp == ""  {
        for i := 0; i<*topX; i++ {
            fmt.Printf("Taxid %v\t: Rank %v\n", t20Sort[i].Taxid, t20Sort[i].AvgRank )
        }
    } else if *rnk && *outp != "" {
        // This file is the same across all samples, copying everywhere anyway
        quit = make(chan struct{})
        go utils.Spinner("Writing average ranks...", 100, quit)
        for idx, _ := range files {
            wg.Add(1)
            go func(outdir, prefix string, t20 rank.Ranks) {
                defer wg.Done()
                t20.Write(outdir, fmt.Sprintf("%v_avg_ranks.out", prefix))
            }(out[idx], lPref[idx], t20Sort)
        }
        wg.Wait()
        close(quit)
        time.Sleep(250 * time.Millisecond)
        fmt.Printf(" Done!\n\n")
    }

    if *res {
        for file, c := range sortedBlast {
            fmt.Fprintf(os.Stdout, "\nFile %v :\n", file)
            for i := 0; i<*topX; i++ {
                fmt.Fprintf(os.Stdout, "Taxid %v\t: %v sequences\n", t20Sort[i].Taxid, len(c[t20Sort[i].Taxid]))
            }
        }
    }

    if *bla != 0 && *outp == "" {
        for file, results := range blastRes {
            fmt.Printf("\nSorted blast for file (top %v results): %v\n", *bla, file)
            fmt.Printf("%v\n", strings.Repeat("-", len(fmt.Sprintf("Sorted blast for file (top %v results): %v", file))))
            results.Pretty(*bla)
            fmt.Printf("%v\n", strings.Repeat("-", 50))
        }
    } else if *bla != 0 && *outp != "" {
        quit = make(chan struct{})
        go utils.Spinner("Writing sorted blasts...", 100, quit)
        for idx, file := range files {
            wg.Add(1)

            go func(outdir, prefix string, b blast.Blast) {
                defer wg.Done()
                b.Write(outdir, fmt.Sprintf("%v_sorted.out", prefix))

            }(out[idx], lPref[idx], blastRes[file])
        }
        wg.Wait()
        close(quit)
        time.Sleep(250 * time.Millisecond)
        fmt.Printf(" Done!\n\n")
    }

    if *dup && *outp == "" {
        for _, file := range files {
            fmt.Printf("\nDuplicated best hits for file : %v\n", file)
            fmt.Printf("--------------------------------------------------\n")
            sortedBlastDup[file].PrintDup()
        }
    } else if *dup && *outp != "" {
        quit = make(chan struct{})
        go utils.Spinner("Writing duplicates...", 100, quit)
        for idx, file := range files {
            wg.Add(1)

            go func(outdir, prefix string, d blast.Duplicated) {
                defer wg.Done()
                d.Write(outdir, fmt.Sprintf("%v_duplicated.out", prefix))

            }(out[idx], lPref[idx], sortedBlastDup[file])
        }
        wg.Wait()
        close(quit)
        time.Sleep(250 * time.Millisecond)
        fmt.Printf(" Done!\n\n")
    }

}
